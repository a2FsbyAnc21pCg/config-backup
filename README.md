# config backup

This repository contains a backup of configurations for linux machine

## External dependencies 

- bumblebee-status
Installation 
```pip3 install bumblebee-status```  - For i3status bar

- pywal
Installation 
```pip3 install pywal``` - For hlwm theming

- Added rofi and custom script for setting wallpaper and theme
```Alt + t``` - opens wallpaper selector (select theme as well).


