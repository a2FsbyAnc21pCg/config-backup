#!/bin/bash

WALLPAPERDIR="/home/kali/Pictures/wallpapers/"
SELECTED="$(find ${WALLPAPERDIR} -type f \( -name "*.jpg" -o -name "*.png" \) -printf "%f\n" | rofi -dmenu -p "Select theme")"
WALLFILE="${WALLPAPERDIR}${SELECTED}"

if test -f "$WALLFILE";then
	killall -q polybar
	echo "$WALLFILE selected"
	# clear pywal cache
	/home/kali/.local/bin/wal -c
	# set pywal colorscheme with custom backend
	# Backend values : haishoku , colorz , colorthief, schemer2
	/home/kali/.local/bin/wal -i "${WALLFILE}" --backend="${GOPATH}/bin/schemer2" -q --saturate 0.6 -n
	# set pywal-discord color theme
	/home/kali/Repos/pywal-discord/pywal-discord
	$HOME/.config/herbstluftwm/polybarlaunch.sh
else
	echo "$WALLFILE does not exist"
fi
